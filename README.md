# Bar Rubble 

```
Quickly generate output for viewing in swaybar or similar

Usage: bar-rubble [COMMAND]

Commands:
  ip         Prints local ip addr
  mail       Print number of unread emails
  mpris      Prints currently playing media eg spotify/youtube
  network    Prints status of network adapter
  razor      Print charge level of razor mouse as a percentage
  weather    Prints Weather. Defaults to --city=portsmouth
  wireguard  Prints Status of WG connection
  help       Print this message or the help of the given subcommand(s)

Options:
  -h, --help     Print help
  -V, --version  Print version
```
