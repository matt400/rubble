use crate::RubbleResult;
use local_ip_address::local_ip;
use serde_json::json;

pub fn print_ip() -> RubbleResult {
    let r = Ok(json!({
    "local_ip": local_ip().unwrap().to_string()
    }));

    RubbleResult::new(r, default_format())
}

crate::avaliable_formattings!("{{local_ip}}", "local_ip");
