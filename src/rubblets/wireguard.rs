use crate::RubbleResult;
use ipnetwork;
use pnet_datalink;
use serde_json::json;

pub fn wg_status() -> RubbleResult {
    let status = if interface().is_none() {
        "Disconnected"
    } else {
        "Connected"
    };

    let r = Ok(json!({
      "status": status,
      "ip": interface().unwrap()
    }));
    RubbleResult::new(r, default_format())
}

use ipnetwork::IpNetwork;
use std::net::IpAddr;

fn maybe_tailscale(s: &str) -> bool {
    s.starts_with("wg")
        || s.starts_with("ts")
        || s.starts_with("tailscale")
        || s.starts_with("utun")
}

/// Retrieve the IP address of the current machine's Tailscale interface, if any.
pub fn interface() -> Option<IpAddr> {
    let ifaces = pnet_datalink::interfaces();
    let netmask: IpNetwork = "100.64.0.0/10".parse().unwrap();
    ifaces
        .iter()
        .filter(|iface| maybe_tailscale(&iface.name))
        .flat_map(|iface| iface.ips.clone())
        .filter(|ipnet| ipnet.is_ipv4() && netmask.contains(ipnet.network()))
        .map(|ipnet| ipnet.ip())
        .next()
}

crate::avaliable_formattings!("{{status}}", "status ip");
