use crate::RubbleResult;
use mpris::{Metadata, PlayerFinder};
use serde_json::json;

pub fn currently_playing() -> RubbleResult {
    let player = PlayerFinder::new()
        .expect("Could not connect to D-Bus")
        .find_active()
        .expect("Could not find any player");

    let metadata: Metadata = player
        .get_metadata()
        .expect("Could not get metadata for player");

    let artist = metadata.artists().unwrap()[0];
    let title = metadata.title().unwrap().to_string();
    let album = metadata.album_name().unwrap().to_string();

    let r = Ok(json!({
      "album": album,
      "artist": artist,
      "title": title,
    }));
    RubbleResult::new(r, default_format())
}

crate::avaliable_formattings!("{{artist}} - {{title}}", "album artist title");
