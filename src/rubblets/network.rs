use crate::RubbleResult;
use clap::Args;
use serde_json::json;
use std::fs;

#[derive(Args)]
pub struct NetworkArgs {
    /// Adapter to use. Defaults to --adapter=eth0
    #[arg(default_value = "eth0", short, long)]
    pub adapter: String,
}

impl NetworkArgs {
    pub fn empty() -> Self {
        Self {
            adapter: String::from(""),
        }
    }
}

pub fn check_status(args: &NetworkArgs) -> RubbleResult {
    let path = format!("/sys/class/net/{}/operstate", args.adapter);
    let status = fs::read_to_string(path);
    let r = match status {
        Ok(status) => Ok(json!({ "status": status })),
        Err(_) => Err("No matching adapter".into()),
    };
    RubbleResult::new(r, default_format())
}

crate::avaliable_formattings!("{{status}}", "status");

// pub fn bar_info(user_format: Option<String>, adapter: String) {
//     crate::utils::printer(user_format, check_status(adapter), default_format())
// }
