use crate::RubbleResult;
use clap::Args;
use glob::glob;
use serde_json::json;
use std::env;
use std::path::Path;

#[derive(Args)]
pub struct MailArgs {
    /// User to check mailcount of
    #[arg(short, long)]
    pub user: Option<String>,
}

impl MailArgs {
    pub fn empty() -> Self {
        Self { user: None }
    }
}

pub fn count(args: &MailArgs) -> RubbleResult {
    let path = if args.user.is_some() {
        format!("/home/{}", args.user.as_ref().unwrap())
    } else {
        format!("/home/{}", whoami::username())
    };

    let home = Path::new(&path);
    let dir_change = env::set_current_dir(home);
    let mut count = 0;
    let r = match dir_change {
        Err(_) => Err("Could not find home dir for user".into()),
        Ok(_) => {
            for entry in glob(".local/share/mail/*/INBOX/new/*").expect("Failed to read glob") {
                if entry.is_err() {
                    count += 1;
                }
            }
            Ok(json!({ "count": count }))
        }
    };
    RubbleResult::new(r, default_format())
}

crate::avaliable_formattings!("{{count}}", "count");

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_default_format() {
        let result = default_format();
        assert_eq!(result, "{{count}}");
    }
}
