use crate::RubbleResult;
use glob::glob;
use serde_json::json;
use std::fs;

fn trim_newline(s: &mut String) {
    if s.ends_with('\n') {
        s.pop();
        if s.ends_with('\r') {
            s.pop();
        }
    }
}

pub fn mouse_battery_level() -> RubbleResult {
    if let Some(entry) = glob("/sys/bus/hid/drivers/razermouse/*/charge_level")
        .expect("failed to read glob")
        .next()
    {
        match entry {
            Ok(path) => {
                let level = fs::read_to_string(path); //.unwrap_or(String::from("0"));
                let mut charge_string = match level {
                    Ok(r) => r,
                    Err(_) => String::from("0"),
                };
                trim_newline(&mut charge_string);
                let charge = charge_string.parse::<i32>().unwrap() * 100 / 255;
                let r = Ok(json!({
                  "charge": charge,
                }));
                return RubbleResult::new(r, default_format());
            }
            Err(_) => {
                return RubbleResult::new(Err("dunno how we got here".into()), default_format());
                // solve
                // this case
            }
        }
    }
    RubbleResult::new(Err("no mouse found".into()), default_format())
}

crate::avaliable_formattings!("{{charge}}%", "charge");
